all: cmake
	$(MAKE) -C build

.PHONY: cmake test test_cases

cmake: build/.cmake_is_lastest 

build/.cmake_is_lastest: CMakeLists.txt
	@mkdir -p build
	@cd build && cmake -DCMAKE_CXX_FLAGS="`llvm-config --cxxflags | sed 's/-std=c++[0-9]*//g'` -std=c++17 -g" -DLLVM_DIR="`llvm-config --cmakedir`" ..
	@touch build/.cmake_is_lastest

test_cases:
	@for f in `ls test_cases/*.c`; do \
		clang -c -g -emit-llvm $$f -o $${f%.c}.bc; \
	done

test: all
	@for f in `ls test_cases/*.bc`; do \
		fname=$${f#test_cases/}; \
		echo "- testing $${fname%.bc}"; \
		build/assignment3 $$f > /dev/null; \
		cat $${f%.bc}.c | grep //; \
	done