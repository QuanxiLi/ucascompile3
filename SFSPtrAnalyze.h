/**
 * This is a staged flow-senstive pointer analyze pass
 * which is based on Hardekopf et, al.
 * CGO 2011 Flow-Sensitive Pointer Analysis for Millions of Lines of Code
 * https://www.cs.utexas.edu/users/lin/papers/cgo11.pdf
 * 
 * Implemented by Quanxi Li
 * liquanxi20@mails.ucas.ac.cn  
*/

#ifndef __SFS_PTR_ANALYZE_H__
#define __SFS_PTR_ANALYZE_H__

#include <llvm/IR/Function.h>
#include <llvm/IR/IntrinsicInst.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

#include "ValOrObj.h"
#include <unordered_map>
#include <unordered_set>

#include "debug.h"
using namespace llvm;

class SFSPtrAnalyzePass : public ModulePass {
private:
    std::unordered_map<ValOrObj, std::unordered_set<ValOrObj, WeakHashOfValOrObj, WeakEqualOfValOrObj>> pts;

public:
    static char ID; // Pass identification, replacement for typeid
public:
    SFSPtrAnalyzePass() : ModulePass(ID) {}

    bool runOnModule(Module &module) override;

    void getAnalysisUsage(AnalysisUsage &AU) const override;

    void outputMsg(const Module &module);
};

#endif