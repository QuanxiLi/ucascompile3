本程序实现了流敏感的指针分析，主要部分为两个LLVM Pass：
- `AndersenStyleAnalyzePass`是基于包含的指针分析，它不是流敏感的，用作`SFSPtrAnalyzePass`的辅助分析；
- `SFSPtrAnalyzePass`是分段的流敏感的指针分析，它基于`AndersenStyleAnalyzePass`的结果进行。

实现上，先用`AndersenStyleAnalyzePass`进行不精确的分析。
随后，基于分析结果建立过程间数据流图（`icfg::ICFG`类），并对所有可取址的对象（Address-taken object，即由`alloca`指令或`malloc`调用等得到的对象）执行SSA算法。
最后在过程间数据流图上进行分析。

注意事项：
- 本程序只考虑直接使用`malloc`/`calloc`/`aligned_alloc`/`memcpy`/`memmove`/`realloc`，或者通过函数指针访问`malloc`/`calloc`/`aligned_alloc`等外部函数的情况。
  对于其他外部函数（包括弱链接函数）的使用，程序将无法正确进行分析。
- 本程序认为，通过`malloc`等分配小于等于`sizeof(void *)`，或者在栈上分配只含一个元素的对象时，该对象不能同时保存多于一个函数指针。
  - 其中，“只含一个元素”的定义为，不属于复合类型，或者是只有一个字段的结构体或数组，其中这个字段也只含一个元素。
  - 参见`icfg::isSingleElementType`
- 本程序对参考文献中的算法有一定改动；同时，没有实现其中`Access Equivalence`算法。

参考文献：
> Hardekopf et, al. CGO 2011, Flow-Sensitive Pointer Analysis for Millions of Lines of Code
