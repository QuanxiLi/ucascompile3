#include <deque>
#include <llvm/IR/CFG.h>
#include <llvm/IR/Instructions.h>
#include <llvm/IR/IntrinsicInst.h>
#include <llvm/IR/Module.h>
#include <memory>
#include <stack>
#include <unordered_map>
#include <unordered_set>

#include "AndersenStyleAnalyze.h"
#include "ValOrObj.h"
#include "debug.h"

namespace icfg {

// abstract instruction
struct AbsInst {
    enum Type {
        Alloc, // alloc is the only way to create an object on memory
        Copy,  // including phi, etc. `ret a` = `copy fn.ret, a`
        Load,
        Store,
        Phi,
        Call,
    };
    Type getType() const { return type; }

protected:
    Type type;
    AbsInst(Type type) : type(type) {}
    AbsInst(const AbsInst &other) = default;
};

struct AllocInst : AbsInst {
    const llvm::Value *val;
    ValOrObj obj;

    AllocInst(const llvm::Value *val, ValOrObj obj)
        : AbsInst(Alloc), val(val), obj(obj) {}
    AllocInst(const AllocInst &other) = default;

    template <class StreamT>
    void print(StreamT &&out) const {
        out << "alloc " << val->getName() << " <- " << obj;
    }
};
struct CopyInst : AbsInst {
    ValOrObj dest;
    std::vector<ValOrObj> src;

    CopyInst(ValOrObj dest)
        : AbsInst(Copy), dest(dest) {}
    CopyInst(const CopyInst &other) = default;

    template <class StreamT>
    void print(StreamT &&out) const {
        out << "copy " << dest << " <-";
        for (auto srcv : src) {
            out << ' ' << srcv;
        }
    }
};

struct LoadInst : AbsInst {
    ValOrObj val;
    const llvm::Value *ptr;
    std::vector<ValOrObj> aux_pts;

    LoadInst(ValOrObj val, const llvm::Value *ptr)
        : AbsInst(Load), val(val), ptr(ptr) {}
    LoadInst(const LoadInst &other) = default;

    template <class StreamT>
    void print(StreamT &&out) const {
        out << "load " << val << " <- " << ptr->getName() << " { ";
        for (auto pts : aux_pts) {
            out << pts << "; ";
        }
        out << " }";
    }
};

struct StoreInst : AbsInst {
    const llvm::Value *ptr;
    ValOrObj val;
    std::vector<std::pair<ValOrObj /* new */, ValOrObj /* old */>> aux_pts;

    StoreInst(const llvm::Value *ptr, ValOrObj val)
        : AbsInst(Store), ptr(ptr), val(val) {}
    StoreInst(const StoreInst &other) = default;

    template <class StreamT>
    void print(StreamT &&out) const {
        out << "store " << ptr->getName() << " <- " << val << " {";
        for (auto &pts : aux_pts) {
            out << ' ' << pts.first << " <- " << pts.second << "; ";
        }
        out << " }";
    }
};

struct PhiInst : AbsInst {
    ValOrObj val;
    std::vector<ValOrObj> src;

    PhiInst(ValOrObj val, size_t src_count)
        : AbsInst(Phi), val(val), src(src_count, val) {}
    PhiInst(const PhiInst &other) = default;

    template <class StreamT>
    void print(StreamT &&out) const {
        out << "phi " << val << " <-";
        for (auto srcv : src) {
            out << ' ' << srcv;
        }
    }
};

struct CallInst : AbsInst {
    const llvm::Value *result;
    const llvm::Value *callee;
    std::vector<const llvm::Value *> args;

    CallInst(const llvm::CallBase *call, const llvm::Value *callee)
        : AbsInst(Call), result(call), callee(callee) {}
    CallInst(const CallInst &other) = default;

    template <class StreamT>
    void print(StreamT &&out) const {
        out << "call " << result->getName() << " <- " << callee->getName() << " (";
        for (auto arg : args) {
            out << arg->getName() << ", ";
        }
        out << ')';
    }
};

template <class StreamT>
void print(const AbsInst *inst, StreamT &&out) {
    switch (inst->getType()) {
    case AbsInst::Alloc:
        static_cast<const AllocInst *>(inst)->print(std::forward<StreamT>(out));
        break;
    case AbsInst::Copy:
        static_cast<const CopyInst *>(inst)->print(std::forward<StreamT>(out));
        break;
    case AbsInst::Load:
        static_cast<const LoadInst *>(inst)->print(std::forward<StreamT>(out));
        break;
    case AbsInst::Store:
        static_cast<const StoreInst *>(inst)->print(std::forward<StreamT>(out));
        break;
    case AbsInst::Phi:
        static_cast<const PhiInst *>(inst)->print(std::forward<StreamT>(out));
        break;
    case AbsInst::Call:
        static_cast<const CallInst *>(inst)->print(std::forward<StreamT>(out));
        break;
    }
}

struct ICFGBB {
    std::vector<int> preds;
    std::vector<int> succs;
    std::deque<std::unique_ptr<AbsInst>> inst_list;

    ICFGBB() {}

    ICFGBB(ICFGBB &&rvalue)
        : preds(std::move(rvalue.preds)), succs(std::move(rvalue.succs)) {
        for (auto &uptr : rvalue.inst_list) {
            inst_list.emplace_back(std::move(uptr));
        }
    }
    template <class StreamT>
    void print(StreamT &&out) const {
        out << ":\t";
        out << "preds=[";
        for (int pred : preds) {
            out << pred << ", ";
        }
        out << "]; succs=[";
        for (int succ : succs) {
            out << succ << ", ";
        }
        out << "]\n";
        for (auto &inst_uptr : inst_list) {
            out << '\t';
            icfg::print(inst_uptr.get(), out);
            out << '\n';
        }
        out << '\n';
    }
    // ---------------- add instructions ----------------

    auto addAlloc(const llvm::Instruction *inst) {
        auto inst_ptr = new AllocInst(inst, ValOrObj::objAllocatedBy(inst));
        inst_list.emplace_back(inst_ptr);
        return inst_ptr;
    }

    auto addCopy(ValOrObj to) {
        auto inst_ptr = new CopyInst(to);
        inst_list.emplace_back(inst_ptr);
        return inst_ptr;
    }

    auto addCopy(ValOrObj to, ValOrObj from) {
        auto inst_ptr = new CopyInst(to);
        inst_ptr->src.push_back(from);
        inst_list.emplace_back(inst_ptr);
        return inst_ptr;
    }

    auto addLoad(const ValOrObj val, const llvm::Value *ptr) {
        auto inst_ptr = new LoadInst(val, ptr);
        inst_list.emplace_back(inst_ptr);
        return inst_ptr;
    }

    auto addStore(const llvm::Value *ptr, ValOrObj val) {
        auto inst_ptr = new StoreInst(ptr, val);
        inst_list.emplace_back(inst_ptr);
        return inst_ptr;
    }

    auto addCall(const llvm::CallBase *call) {
        auto callee = call->getCalledOperand();
        auto inst_ptr = new CallInst(call, callee);
        for (auto &arg : call->args()) {
            inst_ptr->args.emplace_back(arg.get());
        }
        inst_list.emplace_back(inst_ptr);
        return inst_ptr;
    }

    void insertPhi(ValOrObj obj) {
        auto inst_ptr = new PhiInst(obj, preds.size());
        inst_list.emplace_front(inst_ptr);
    }

    // ---------------- build ICFG ----------------

    void addPred(int pred) {
        preds.push_back(pred);
    }

    void addSucc(int succ) {
        succs.push_back(succ);
    }
};

// the interprocedural control-flow graph
class ICFG {
public:
    typedef AndersenStyleAnalyzePass AUX;
    const llvm::Module &module;
    const AUX::ResultType &aux_result;
    std::vector<ICFGBB> basic_blocks;
    std::unordered_map<const llvm::Function *, int> func_entry;
    std::unordered_map<const llvm::Function *, int> func_exit;
    std::unordered_map<const llvm::BasicBlock *, int> llvmbb_entry;
    std::unordered_map<const llvm::BasicBlock *, int> llvmbb_exit;
    std::unordered_map<int, int> after_calling; // if B ends with a call, it will return back to after_calling[B]
    std::unordered_set<ValOrObj> address_taken_objs;
    std::unordered_set<ValOrObj> single_element_objs; // an address-taken object with only ONE object, I can use strong update on it.

public:
    ICFG(const llvm::Module &module, const AUX::ResultType &aux_result)
        : module(module), aux_result(aux_result) {
        createBasicBlocks();
        createCFG();
        makeAddressTakenObjSAA();
        debug() << "-------- ICFG --------\n";
        for (int i = 0; i < basic_blocks.size(); i++) {
            debug() << "BB #" << i;
            basic_blocks[i].print(debug());
        }
        debug() << "\n";
    }

private:
    void createBasicBlocks();

    void createCFG();

    void makeAddressTakenObjSAA();

    void addEdge(int from, int to) {
        basic_blocks[from].addSucc(to);
        basic_blocks[to].addPred(from);
    }

    void _ssaRename(int bb_index, std::unordered_set<int> *dom_tree,
                    std::unordered_map<ValOrObj, std::stack<int>, WeakHashOfValOrObj, WeakEqualOfValOrObj> &rename_ids,
                    int &current_id);
};

}; // namespace icfg