#include "AndersenStyleAnalyze.h"
#include <map>

using namespace llvm;

char AndersenStyleAnalyzePass::ID = 0;
static RegisterPass<AndersenStyleAnalyzePass> X("andersen", "Make Andersen style analyze");

bool AndersenStyleAnalyzePass::runOnModule(Module &module) {
    for (const auto &fn : module) {
        initialize(&fn);
        for (auto &bb : fn) {
            for (auto &inst : bb) {
                initialize(&inst);
            }
        }
    }
    work();
#ifdef DEBUG_OUTPUT
    printResult();
    outputMsg(module);
#endif
    return false;
}

void AndersenStyleAnalyzePass::initialize(const Function *fn) {
    worklist[fn].insert(ValOrObj::functionObjOf(fn));
}

void AndersenStyleAnalyzePass::initialize(const Instruction *inst) {
    if (isa<DbgInfoIntrinsic>(inst))
        return; // not considering instrinsic debug functions
    if (isa<AllocaInst>(inst)) {
        // alloca; malloc, ...
        worklist[inst].insert(ValOrObj::objAllocatedBy(inst));
    } else if (auto load = dyn_cast<LoadInst>(inst)) {
        // load
        auto src = load->getPointerOperand();
        loads[src].push_back(inst);
    } else if (auto store = dyn_cast<StoreInst>(inst)) {
        // store
        auto dest = store->getPointerOperand();
        auto src = store->getValueOperand();
        stores[dest].push_back(src);
    } else if (auto call = dyn_cast<CallBase>(inst)) {
        // call
        auto callee = call->getCalledValue();
        if (auto called_fn = dyn_cast<Function>(callee)) {
            addEdgeForCall(call, called_fn);
        } else {
            calls[callee].push_back(call);
        }
    } else if (auto phi = dyn_cast<PHINode>(inst)) {
        // phi
        for (auto &use : phi->incoming_values()) {
            addEdge(use.get(), phi);
        }
    } else if (auto select = dyn_cast<SelectInst>(inst)) {
        // select
        addEdge(select->getTrueValue(), select);
        addEdge(select->getFalseValue(), select);
    } else if (auto gepi = dyn_cast<GetElementPtrInst>(inst)) {
        // getelementptr
        addEdge(gepi->getPointerOperand(), gepi);
    } else if (auto cast = dyn_cast<CastInst>(inst)) {
        // cast
        addEdge(cast->getOperand(0), cast);
    } else if (auto ret = dyn_cast<ReturnInst>(inst)) {
        // ret
        addEdge(ret->getReturnValue(), ValOrObj::retValueOf(ret->getFunction()));
    }
}

void AndersenStyleAnalyzePass::work() {
    while (!worklist.empty()) {
        // here we'll try to add some more objects to point-to set of v
        // get (v, pts) from worklist
        auto it = worklist.begin();
        ValOrObj v = it->first;
        auto pts = std::move(it->second);
        worklist.erase(it);
        // get the new point-to objects of v, as new_pts
        auto &ptsv = point_to_set[v];
        std::vector<ValOrObj> new_pts;
        for (auto x : pts) {
            if (ptsv.find(x) == ptsv.end()) {
                ptsv.insert(x);
                new_pts.push_back(x);
            }
        }
        // propagate
        for (auto to : pointer_flow_graph[v]) {
            for (auto npt : new_pts) {
                worklist[to].insert(npt);
            }
        }
        if (v.type == ValOrObj::Val) {
            for (auto np : new_pts) {
                for (auto src : stores[v.val]) {
                    addEdge(src, np);
                }
                for (auto dest : loads[v.val]) {
                    addEdge(np, dest);
                }
                for (auto call : calls[v.val]) {
                    if (auto called_fn = np.functionObj()) {
                        addEdgeForCall(call, called_fn);
                    }
                }
            }
        }
    }
}

void AndersenStyleAnalyzePass::addEdge(ValOrObj from, ValOrObj to) {
    bool changed = pointer_flow_graph[from].insert(to).second;
    if (changed) {
        for (auto pt : point_to_set[from]) {
            worklist[to].insert(pt);
        }
    }
}

void AndersenStyleAnalyzePass::addEdgeForCall(const CallBase *call, const Function *called_fn) {
    if (called_fn->hasExactDefinition()) {
        addEdge(ValOrObj::retValueOf(called_fn), call);
        for (int i = 0; i < call->arg_size(); i++) {
            addEdge(call->getArgOperand(i), called_fn->getArg(i));
        }
    } else {
        handleExternalFunction(call, called_fn);
    }
}

void AndersenStyleAnalyzePass::handleExternalFunction(const CallBase *call, const Function *called_fn) {
    auto fname = called_fn->getName();
    if (fname == "malloc" || fname == "calloc" || fname == "aligned_alloc") {
        worklist[call].insert(ValOrObj::objAllocatedBy(call));
    } else if (fname == "realloc") {
        addEdge(call->getArgOperand(0), call);
    } else if (fname == "memset" || fname.startswith("llvm.memset.")) {
        return;
    } else if (fname == "memcpy" || fname.startswith("llvm.memcpy.") || fname == "memmove" || fname.startswith("llvm.memmove.")) {
        ValOrObj temp(call, ValOrObj::MemcpyTemp);
        loads[call->getArgOperand(1)].push_back(temp);
        stores[call->getArgOperand(0)].push_back(temp);
    }
}

void AndersenStyleAnalyzePass::printResult() {
#ifdef DEBUG_OUTPUT
    debug() << "Point-to Set:\n";
    for (auto &kv : point_to_set) {
        debug() << '\t' << kv.first << ':';
        for (auto t : kv.second) {
            debug() << ' ' << t;
        }
        debug() << '\n';
    }
#endif
}

void AndersenStyleAnalyzePass::printPFG() {
#ifdef DEBUG_OUTPUT
    debug() << "Pointer Flow Graph:\n";
    for (auto &kv : pointer_flow_graph) {
        debug() << '\t' << kv.first << " ->";
        for (auto t : kv.second) {
            debug() << ' ' << t;
        }
        debug() << '\n';
    }
#endif
}

void AndersenStyleAnalyzePass::outputMsg(const Module &module) {
    std::map<int, std::unordered_set<const Function *>> msg;
    for (auto &fn : module) {
        for (auto &bb : fn) {
            for (auto &inst : bb) {
                if (isa<DbgInfoIntrinsic>(inst))
                    continue; // not considering instrinsic debug functions
                if (auto call = dyn_cast<CallBase>(&inst)) {
                    int line_n = 0;
                    if (auto loc = call->getDebugLoc()) {
                        line_n = loc.getLine();
                    }
                    auto callee = call->getCalledValue();
                    auto &fset = msg[line_n];
                    if (auto fn = dyn_cast<Function>(callee)) {
                        if (fn->isIntrinsic())
                            continue;
                        fset.insert(fn);
                    } else {
                        const auto &possible_val = point_to_set[callee];
                        for (auto it = possible_val.cbegin(); it != possible_val.cend(); it++) {
                            if (auto fn = it->functionObj()) {
                                fset.insert(fn);
                            }
                        }
                    }
                }
            }
        }
    }
    for (auto &kv : msg) {
        if (kv.second.empty())
            continue;
        errs() << kv.first << " : ";
        for (auto it = kv.second.begin(); it != kv.second.end(); it++) {
            if (it != kv.second.begin()) {
                errs() << ", ";
            }
            errs() << (*it)->getName();
        }
        errs() << '\n';
    }
}
