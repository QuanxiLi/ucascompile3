#include "SFSPtrAnalyze.h"
#include "AndersenStyleAnalyze.h"
#include "icfg.h"

char SFSPtrAnalyzePass::ID = 0;
static RegisterPass<SFSPtrAnalyzePass> X("sfsptr", "Make staged flow-senstive pointer analyze");

bool SFSPtrAnalyzePass::runOnModule(Module &module) {
    auto &aux = getAnalysis<AndersenStyleAnalyzePass>();
    auto &aux_result = aux.getResult();
    icfg::ICFG cfg(module, aux_result);

    std::unordered_map<ValOrObj, std::unordered_set<icfg::AbsInst *>> use;
    std::unordered_set<icfg::AbsInst *> worklist;
    for (auto &bb : cfg.basic_blocks) {
        for (auto &inst_uptr : bb.inst_list) {
            switch (inst_uptr->getType()) {
            case icfg::AbsInst::Alloc: {
                auto alloc = static_cast<icfg::AllocInst *>(inst_uptr.get());
                worklist.insert(alloc);
            } break;
            case icfg::AbsInst::Copy: {
                auto copy = static_cast<icfg::CopyInst *>(inst_uptr.get());
                for (auto src : copy->src) {
                    use[src].insert(copy);
                }
            } break;
            case icfg::AbsInst::Load: {
                auto load = static_cast<icfg::LoadInst *>(inst_uptr.get());
                use[load->ptr].insert(load);
                for (auto pt : load->aux_pts) {
                    use[pt].insert(load);
                }
            } break;
            case icfg::AbsInst::Store: {
                auto store = static_cast<icfg::StoreInst *>(inst_uptr.get());
                use[store->ptr].insert(store);
                use[store->val].insert(store);
                for (auto &pt_pair : store->aux_pts) {
                    use[pt_pair.second].insert(store);
                }
            } break;
            case icfg::AbsInst::Phi: {
                auto phi = static_cast<icfg::PhiInst *>(inst_uptr.get());
                for (auto src : phi->src) {
                    use[src].insert(phi);
                }
            } break;
            case icfg::AbsInst::Call: {
                auto call = static_cast<icfg::CallInst *>(inst_uptr.get());
                use[call->callee].insert(call);
                for (auto arg : call->args) {
                    use[arg].insert(call);
                }
            } break;
            }
        }
    }

    for (auto &fn : module) {
        pts[&fn].insert(ValOrObj::functionObjOf(&fn));
        for (auto n : use[&fn]) {
            worklist.insert(n);
        }
    }

    while (!worklist.empty()) {
        auto inst = *worklist.begin();
        worklist.erase(worklist.begin());
        debug() << "working on: ";
        icfg::print(inst, debug());
        debug() << '\n';
        switch (inst->getType()) {
        case icfg::AbsInst::Alloc: {
            auto alloc = static_cast<icfg::AllocInst *>(inst);
            bool changed = pts[alloc->val].insert(alloc->obj).second;
            if (changed) {
                for (auto n : use[alloc->val]) {
                    worklist.insert(n);
                }
            }
        } break;
        case icfg::AbsInst::Copy: {
            auto copy = static_cast<icfg::CopyInst *>(inst);
            bool changed = false;
            auto &pts_dest = pts[copy->dest];
            for (auto src : copy->src) {
                for (auto src_pt : pts[src]) {
                    changed = pts_dest.insert(src_pt).second || changed;
                }
            }
            if (changed) {
                for (auto n : use[copy->dest]) {
                    worklist.insert(n);
                }
            }
        } break;
        case icfg::AbsInst::Load: {
            auto load = static_cast<icfg::LoadInst *>(inst);
            bool changed = false;
            auto &pts_val = pts[load->val];
            auto &actual_pts = pts[load->ptr]; // CAN NOT use the value in `actual_pts` directly because it use the `weak equivalence`
            for (auto ptr_pt : load->aux_pts) {
                // ptr_pt is the obj that load->ptr may points to according to `aux`
                // however, it may be not accurate
                if (actual_pts.find(ptr_pt) == actual_pts.end()) {
                    continue;
                }
                // we confirmed that ptr_pt is in the point to set
                for (auto pt : pts[ptr_pt]) {
                    changed = pts_val.insert(pt).second || changed;
                }
            }
            if (changed) {
                for (auto n : use[load->val]) {
                    worklist.insert(n);
                }
            }
        } break;
        case icfg::AbsInst::Store: {
            auto store = static_cast<icfg::StoreInst *>(inst);
            auto &actual_pts = pts[store->ptr];
            if (actual_pts.size() == 1 && cfg.single_element_objs.find(*actual_pts.begin()) != cfg.single_element_objs.end()) {
                auto actual_pt = *actual_pts.begin();
                bool __debug_updated = false;
                bool changed = false;
                for (auto &aux_pt : store->aux_pts) {
                    if (WeakEqualOfValOrObj{}(aux_pt.first, actual_pt)) {
                        // update aux_pt.first
                        __debug_updated = true;
                        auto &pts_dest = pts[aux_pt.first];
                        for (auto pt : pts[store->val]) {
                            changed = pts_dest.insert(pt).second || changed;
                        }
                        if (changed) {
                            for (auto n : use[aux_pt.first]) {
                                worklist.insert(n);
                            }
                        }
                        break;
                    }
                }
                assert(__debug_updated);
            } else {
                // weak update
                bool changed = false;
                for (auto &aux_pt : store->aux_pts) {
                    if (actual_pts.find(aux_pt.first) != actual_pts.end()) {
                        auto &pts_dest = pts[aux_pt.first];
                        for (auto pt : pts[aux_pt.second]) { // old values
                            changed = pts_dest.insert(pt).second || changed;
                        }
                        for (auto pt : pts[store->val]) {
                            changed = pts_dest.insert(pt).second || changed;
                        }
                        if (changed) {
                            for (auto n : use[aux_pt.first]) {
                                worklist.insert(n);
                            }
                        }
                    }
                }
            }
        } break;
        case icfg::AbsInst::Phi: {
            auto phi = static_cast<icfg::PhiInst *>(inst);
            auto &pts_val = pts[phi->val];
            bool changed = false;
            for (auto src : phi->src) {
                for (auto pt : pts[src]) {
                    changed = pts_val.insert(pt).second || changed;
                }
            }
            if (changed) {
                for (auto n : use[phi->val]) {
                    worklist.insert(n);
                }
            }
        } break;
        case icfg::AbsInst::Call: {
            auto call = static_cast<icfg::CallInst *>(inst);
            auto &pts_result = pts[call->result];
            bool changed = false;
            for (auto callee : pts[call->callee]) {
                if (auto fn = callee.functionObj()) {
                    if (fn->arg_size() != call->args.size()) {
#ifdef DEBUG_OUTPUT
                        debug() << "WARNNING: ";
                        debug() << "fn->arg_size() != call->args.size()\n";
                        debug() << "\tfn: " << fn->getName() << " with " << fn->arg_size() << "args\n";
                        debug() << "\tcall: ";
                        call->print(debug());
                        debug() << " with " << call->args.size() << "args\n";
                        goto PRINT_POINT_TO_SET;
#else
                        continue;
#endif
                    }
                    if (!fn->hasExactDefinition()) {
                        auto fname = fn->getName();
                        if (fname == "malloc" || fname == "calloc" || fname == "aligned_alloc") {
                            changed = pts_result.insert(ValOrObj::objAllocatedBy(dyn_cast<Instruction>(call->result))).second || changed;
                        } else {
                            errs() << "ERROR: calling a function pointer which may points to function '"
                                   << fname << "' without exact definition. "
                                   << "The pointer analyzer can not analyze it.\n";
                            continue;
                        }
                    }
                    auto retval = ValOrObj::retValueOf(fn);
                    // add use to the return value
                    use[retval].insert(call);
                    // update args
                    for (int i = 0; i < call->args.size(); i++) {
                        bool arg_changed = false;
                        auto fn_argi = fn->getArg(i);
                        for (auto pt : pts[call->args[i]]) {
                            arg_changed = pts[fn_argi].insert(pt).second || arg_changed;
                        }
                        if (arg_changed) {
                            for (auto n : use[fn_argi]) {
                                worklist.insert(n);
                            }
                        }
                    }
                    // update result
                    for (auto pt : pts[retval]) {
                        changed = pts_result.insert(pt).second || changed;
                    }
                }
            }
            if (changed) {
                for (auto n : use[call->result]) {
                    worklist.insert(n);
                }
            }
            // TODO: some phi may be changed
        } break;
        }
    }

#ifdef DEBUG_OUTPUT
PRINT_POINT_TO_SET:
    debug() << "--------------------------------\n";
    debug() << "point-to set:\n";
    for (auto &kv : pts) {
        debug() << '\t' << kv.first << ": ";
        for (auto pt : kv.second) {
            debug() << pt << ", ";
        }
        debug() << '\n';
    }
    debug() << "--------------------------------\n";
#endif // DEBUG_OUTPUT

    outputMsg(module);
    return false;
}

void SFSPtrAnalyzePass::getAnalysisUsage(AnalysisUsage &AU) const {
    AU.addRequired<AndersenStyleAnalyzePass>();
    AU.setPreservesAll();
}

void SFSPtrAnalyzePass::outputMsg(const Module &module) {
    std::map<int, std::unordered_set<const Function *>> msg;
    for (auto &fn : module) {
        for (auto &bb : fn) {
            for (auto &inst : bb) {
                if (isa<DbgInfoIntrinsic>(inst))
                    continue; // not considering instrinsic debug functions
                if (auto call = dyn_cast<CallBase>(&inst)) {
                    int line_n = 0;
                    if (auto loc = call->getDebugLoc()) {
                        line_n = loc.getLine();
                    }
                    auto callee = call->getCalledValue();
                    auto &fset = msg[line_n];
                    if (auto fn = dyn_cast<Function>(callee)) {
                        if (fn->isIntrinsic())
                            continue;
                        fset.insert(fn);
                    } else {
                        const auto &possible_val = pts[callee];
                        for (auto it = possible_val.cbegin(); it != possible_val.cend(); it++) {
                            if (auto fn = it->functionObj()) {
                                fset.insert(fn);
                            }
                        }
                    }
                }
            }
        }
    }
    for (auto &kv : msg) {
        if (kv.second.empty())
            continue;
        errs() << kv.first << " : ";
        for (auto it = kv.second.begin(); it != kv.second.end(); it++) {
            if (it != kv.second.begin()) {
                errs() << ", ";
            }
            errs() << (*it)->getName();
        }
        errs() << '\n';
    }
}
