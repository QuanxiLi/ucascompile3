#ifndef __VAL_OR_OBJ_H__
#define __VAL_OR_OBJ_H__

#include <llvm/IR/Function.h>
#include <llvm/IR/Instruction.h>
#include <llvm/IR/Value.h>
#include <llvm/Support/raw_ostream.h>

struct ValOrObj {
    enum ObjType {
        Val,        // the top level (SSA-) val of llvm
        Obj,        // an object created on memory (by alloca / malloc / etc)
        RetVal,     // the return value of a function
        FnObj,      // a function, note that the symbol of a function in llvm should be a `Val` type which points to a `FnObj` type
        MemcpyTemp, // temp obj for call of memcpy
    };

    ObjType type;
    int ssa_rename_id;
    const llvm::Value *val;

    ValOrObj(const ValOrObj &) = default;
    ValOrObj(const ValOrObj &other, int ssa_ername_id)
        : type(other.type), ssa_rename_id(ssa_rename_id), val(other.val) {}
    ValOrObj(const llvm::Value *val) : type(Val), val(val), ssa_rename_id(0) {}
    ValOrObj(const llvm::Value *val, ObjType type) : type(type), val(val), ssa_rename_id(0) {}
    ValOrObj &operator=(const ValOrObj &other) {
        type = other.type;
        val = other.val;
        ssa_rename_id = other.ssa_rename_id;
        return *this;
    }
    bool operator==(const ValOrObj &other) const {
        return val == other.val && type == other.type && ssa_rename_id == other.ssa_rename_id;
    }
    static ValOrObj retValueOf(const llvm::Function *fn) {
        return ValOrObj(fn, RetVal);
    }
    static ValOrObj functionObjOf(const llvm::Function *fn) {
        return ValOrObj(fn, FnObj);
    }
    static ValOrObj objAllocatedBy(const llvm::Instruction *inst) {
        return ValOrObj(inst, Obj);
    }
    // return the function if it is a function object; otherwise return nullptr
    const llvm::Function *functionObj() const {
        if (type != FnObj)
            return nullptr;
        return llvm::dyn_cast<llvm::Function>(val);
    }
};

inline llvm::raw_ostream &operator<<(llvm::raw_ostream &out, const ValOrObj &v) {
    if (v.val == nullptr) {
        return out << "null";
    }
    switch (v.type) {
    case ValOrObj::Obj:
        out << "[obj ";
        break;
    case ValOrObj::RetVal:
        out << "[ret ";
        break;
    case ValOrObj::FnObj:
        out << "[fn ";
        break;
    case ValOrObj::MemcpyTemp:
        out << "[memcpytemp ";
        break;
    }
    if (auto arg = llvm::dyn_cast<llvm::Argument>(v.val)) {
        out << arg->getParent()->getName() << '.';
    } else if (auto inst = llvm::dyn_cast<llvm::Instruction>(v.val)) {
        out << inst->getFunction()->getName() << '.';
    }
    if (v.val->hasName()) {
        out << v.val->getName();
    } else {
        out << '?';
    }
    if (v.type != ValOrObj::Val) {
        out << ']';
    }
    if (v.ssa_rename_id != 0) {
        out << "(" << v.ssa_rename_id << ")";
    }
    return out;
}

namespace std {
template <>
struct hash<ValOrObj> {
    typedef ValOrObj argument_type;
    typedef std::size_t result_type;
    result_type operator()(argument_type const &x) const {
        return std::hash<const llvm::Value *>{}(x.val) + static_cast<result_type>(x.type) + (((unsigned)x.ssa_rename_id & 0xFFFF) << 16);
    }
};

}; // namespace std

struct WeakHashOfValOrObj {
    typedef ValOrObj argument_type;
    typedef std::size_t result_type;
    result_type operator()(argument_type const &x) const {
        return std::hash<const llvm::Value *>{}(x.val) + static_cast<result_type>(x.type);
    }
};

// weak equivalence, not considering the ssa rename id
struct WeakEqualOfValOrObj {
    constexpr bool operator()(const ValOrObj &lhs, const ValOrObj &rhs) const {
        return rhs.val == rhs.val && lhs.type == rhs.type;
    }
};

#endif // __VAL_OR_OBJ_H__