#ifndef __ANDERSEN_STYLE_ANALYZE_H__
#define __ANDERSEN_STYLE_ANALYZE_H__

#include <llvm/IR/Function.h>
#include <llvm/IR/IntrinsicInst.h>
#include <llvm/IR/Module.h>
#include <llvm/Pass.h>
#include <llvm/Support/raw_ostream.h>

#include <unordered_map>
#include <unordered_set>

#include "ValOrObj.h"
#include "debug.h"

class AndersenStyleAnalyzePass : public llvm::ModulePass {
private:
    std::unordered_map<ValOrObj, std::unordered_set<ValOrObj>> point_to_set;
    std::unordered_map<ValOrObj, std::unordered_set<ValOrObj>> pointer_flow_graph;
    std::unordered_map<ValOrObj, std::unordered_set<ValOrObj>> worklist;
    std::unordered_map<const llvm::Value * /* dest */, std::vector<ValOrObj> /* src */> stores;
    std::unordered_map<const llvm::Value * /* src */, std::vector<ValOrObj> /* dest */> loads;
    std::unordered_map<const llvm::Value * /* callee */, std::vector<const llvm::CallBase *> /* inst */> calls;

public:
    static char ID; // Pass identification, replacement for typeid
    typedef decltype(point_to_set) ResultType;

public:
    AndersenStyleAnalyzePass() : ModulePass(ID) {}

    void initialize(const llvm::Function *inst);
    void initialize(const llvm::Instruction *inst);

    void addEdge(ValOrObj from, ValOrObj to);

    void addEdgeForCall(const llvm::CallBase *call, const llvm::Function *called_fn);

    void handleExternalFunction(const llvm::CallBase *call, const llvm::Function *called_fn);

    void work();

    bool runOnModule(llvm::Module &module) override;

    const ResultType &getResult() const {
        return point_to_set;
    }

    void printResult();

    void printPFG();

    void outputMsg(const llvm::Module &module);

    void getAnalysisUsage(llvm::AnalysisUsage &AU) const override {
        AU.setPreservesAll();
    }
};

#endif // __ANDERSEN_STYLE_ANALYZE_H__