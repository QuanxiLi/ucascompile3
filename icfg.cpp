#include "icfg.h"
#include <queue>
#include <stack>

using llvm::dyn_cast;
using llvm::isa;

namespace icfg {

bool isSingleElementType(const llvm::Type *type) {
    static std::unordered_map<const llvm::Type *, bool> cache;
    if (auto struct_type = dyn_cast<llvm::StructType>(type)) {
        auto it = cache.find(type);
        if (it != cache.end())
            return it->second;
        if (struct_type->elements().size() > 1) {
            cache[type] = false;
            return false;
        } else {
            auto t = struct_type->elements()[0];
            auto result = isSingleElementType(t);
            cache[type] = result;
            return result;
        }
    } else if (isa<llvm::VectorType>(type)) {
        return false;
    } else if (auto array_type = dyn_cast<llvm::ArrayType>(type)) {
        if (array_type->getNumElements() > 1)
            return false;
        return isSingleElementType(array_type->getElementType());
    }
    return true;
}

void ICFG::createBasicBlocks() {
    debug() << "creating icfg basic blocks...\n";
    for (auto &fn : module) {
        if (!fn.hasExactDefinition())
            continue;
        int retbb_index;
        int ret_count = 0;
        for (auto &llvm_bb : fn) {
            // create bb
            basic_blocks.emplace_back();
            auto bb = &basic_blocks.back();
            int bb_index = basic_blocks.size() - 1;
            // set entry of llvm symbols
            if (&llvm_bb == &fn.getEntryBlock()) {
                func_entry[&fn] = bb_index;
            }
            llvmbb_entry[&llvm_bb] = bb_index;
            // process instructions
            for (auto &inst : llvm_bb) {
                // TODO: discard instructions that is not related to any pointer
                if (isa<llvm::DbgInfoIntrinsic>(&inst))
                    continue; // not considering instrinsic debug functions
                if (auto alloca = dyn_cast<llvm::AllocaInst>(&inst)) {
                    // alloca
                    bb->addAlloc(alloca);
                    auto array_size = dyn_cast<llvm::ConstantInt>(alloca->getArraySize());
                    if (array_size && array_size->isOne()) {
                        auto type = alloca->getAllocatedType();
                        if (isSingleElementType(type)) {
                            auto obj = ValOrObj::objAllocatedBy(alloca);
                            single_element_objs.insert(obj);
                            debug() << "FOUND SINGLE ELEMENT OBJ\n";
                        }
                    }
                } else if (auto load = dyn_cast<llvm::LoadInst>(&inst)) {
                    // load
                    auto ptr = load->getPointerOperand();
                    auto load_inst = bb->addLoad(load, ptr);
                    auto pts_it = aux_result.find(ptr);
                    if (pts_it != aux_result.end()) {
                        for (ValOrObj pt : pts_it->second) {
                            load_inst->aux_pts.push_back(pt);
                            address_taken_objs.insert(pt);
                        }
                    }
                } else if (auto store = dyn_cast<llvm::StoreInst>(&inst)) {
                    // store
                    auto ptr = store->getPointerOperand();
                    auto value = store->getValueOperand();
                    auto store_inst = bb->addStore(ptr, value);
                    auto pts_it = aux_result.find(ptr);
                    if (pts_it != aux_result.end()) {
                        for (ValOrObj pt : pts_it->second) {
                            store_inst->aux_pts.emplace_back(pt, pt);
                            address_taken_objs.insert(pt);
                        }
                    }
                } else if (auto call = dyn_cast<llvm::CallBase>(&inst)) {
                    // call
                    auto called_fn = call->getCalledFunction();
                    if (called_fn && !called_fn->hasExactDefinition()) {
                        // if it calls an external function
                        auto fname = called_fn->getName();
                        if (fname == "malloc" || fname == "calloc" || fname == "aligned_alloc") {
                            bb->addAlloc(call);
                            auto size = dyn_cast<llvm::ConstantInt>(call->getArgOperand(0));
                            if (size) {
                                auto ptr_size = module.getDataLayout().getPointerSize();
                                if (size->getValue().ule(ptr_size)) {
                                    single_element_objs.insert(ValOrObj::objAllocatedBy(call));
                                }
                            }
                        } else if (fname == "realloc") {
                            bb->addCopy(call, call->getArgOperand(0));
                        } else if (fname == "memset" || fname.startswith("llvm.memset.")) {
                            // do nothing
                        } else if (fname == "memcpy" || fname.startswith("llvm.memcpy.") || fname == "memmove" || fname.startswith("llvm.memmove.")) {
                            ValOrObj temp(call, ValOrObj::MemcpyTemp);
                            auto load_ptr = call->getArgOperand(1);
                            auto load_inst = bb->addLoad(temp, load_ptr);
                            auto load_pts_it = aux_result.find(load_ptr);
                            if (load_pts_it != aux_result.end()) {
                                for (ValOrObj pt : load_pts_it->second) {
                                    load_inst->aux_pts.push_back(pt);
                                    address_taken_objs.insert(pt);
                                }
                            }
                            auto store_ptr = call->getArgOperand(0);
                            auto store_inst = bb->addStore(store_ptr, temp);
                            auto store_pts_it = aux_result.find(store_ptr);
                            if (store_pts_it != aux_result.end()) {
                                for (ValOrObj pt : store_pts_it->second) {
                                    store_inst->aux_pts.emplace_back(pt, pt);
                                    address_taken_objs.insert(pt);
                                }
                            }
                        }
                    } else {
                        bb->addCall(call);
                        // create a new bb
                        basic_blocks.emplace_back();
                        after_calling[bb_index] = basic_blocks.size() - 1;
                        bb = &basic_blocks.back();
                        bb_index = basic_blocks.size() - 1;
                    }
                } else if (auto phi = dyn_cast<llvm::PHINode>(&inst)) {
                    // phi
                    auto copy_inst = bb->addCopy(phi);
                    for (auto &use : phi->incoming_values()) {
                        copy_inst->src.emplace_back(use.get());
                    }
                } else if (auto select = dyn_cast<llvm::SelectInst>(&inst)) {
                    // select
                    auto copy_inst = bb->addCopy(select);
                    copy_inst->src.emplace_back(select->getTrueValue());
                    copy_inst->src.emplace_back(select->getFalseValue());
                } else if (auto gepi = dyn_cast<llvm::GetElementPtrInst>(&inst)) {
                    // getelementptr
                    bb->addCopy(gepi, gepi->getPointerOperand());
                } else if (auto cast = dyn_cast<llvm::CastInst>(&inst)) {
                    // cast
                    bb->addCopy(cast, cast->getOperand(0));
                } else if (auto ret = dyn_cast<llvm::ReturnInst>(&inst)) {
                    // ret
                    bb->addCopy(ValOrObj::retValueOf(&fn), ret->getReturnValue());
                    retbb_index = bb_index;
                    ret_count++;
                }
            }
            // set exit of llvm_bb
            llvmbb_exit[&llvm_bb] = bb_index;
        }
        // count of the entry and exit bb of a function should be exactly ONE
        if (ret_count == 1) {
            func_exit[&fn] = retbb_index;
        } else {
            basic_blocks.emplace_back();
            func_exit[&fn] = basic_blocks.size() - 1;
        }
    }
}

void ICFG::createCFG() {
    debug() << "creating icfg...\n";
    for (auto &fn : module) {
        for (auto &llvm_bb : fn) {
            auto exit_bb = llvmbb_exit[&llvm_bb];
            auto terminator = llvm_bb.getTerminator();
            if (isa<llvm::ReturnInst>(terminator)) {
                auto fn_exit_bb = func_exit[&fn];
                if (exit_bb != fn_exit_bb) {
                    addEdge(exit_bb, fn_exit_bb);
                }
            } else {
                for (auto succ : successors(terminator)) {
                    addEdge(exit_bb, llvmbb_entry[succ]);
                }
            }
        }
    }
    for (int i = 0; i < basic_blocks.size(); i++) {
        auto &bb = basic_blocks[i];
        if (bb.inst_list.empty())
            continue;
        auto &end_inst = bb.inst_list.back();
        if (end_inst->getType() == AbsInst::Call) {
            auto callee = static_cast<CallInst *>(end_inst.get())->callee;
            if (auto called_fn = dyn_cast<llvm::Function>(callee)) {
                addEdge(i, func_entry[called_fn]);
                assert(after_calling[i] == i + 1);
                addEdge(func_exit[called_fn], after_calling[i]);
            } else {
                for (auto called_obj : aux_result.at(callee)) {
                    if (auto called_fn = called_obj.functionObj()) {
                        // TODO: what if called_fn is external?
                        assert(called_fn != nullptr);
                        addEdge(i, func_entry[called_fn]);
                        assert(after_calling[i] == i + 1);
                        addEdge(func_exit[called_fn], after_calling[i]);
                    }
                }
            }
        }
    }
}

void ICFG::makeAddressTakenObjSAA() {
    // -------- compute domination --------
    debug() << "compute domination...\n";
    auto dom_uniqueptr = std::make_unique<std::unordered_set<int>[]>(basic_blocks.size());
    auto dom = dom_uniqueptr.get();
    {
        for (auto kv : func_entry) {
            int index = kv.second;
            assert(index >= 0 && index < basic_blocks.size());
            dom[index].insert(index);
        }
        for (int i = 0; i < basic_blocks.size(); i++) {
            if (dom[i].empty()) { // not the function entry
                for (int j = 0; j < basic_blocks.size(); j++) {
                    dom[i].insert(j);
                }
            }
        }
        bool change = true;
        while (change) {
            change = false;
            for (int i = 0; i < basic_blocks.size(); i++) {
                auto &bb = basic_blocks[i];
                if (bb.preds.empty()) {
                    if (dom[i].size() != 1) {
                        dom[i].clear();
                        dom[i].insert(i);
                        change = true;
                    }
                    continue;
                }
                for (auto pred : bb.preds) {
                    assert(pred >= 0 && pred < basic_blocks.size());
                    auto &dom_pred = dom[pred];
                    for (auto dit = dom[i].begin(); dit != dom[i].end();) {
                        if (*dit != i && dom_pred.find(*dit) == dom_pred.end()) {
                            auto dit_ = dit;
                            dit++;
                            dom[i].erase(dit_);
                            change = true;
                        } else {
                            dit++;
                        }
                    }
                }
            }
        }
    }

    for (int i = 0; i < basic_blocks.size(); i++) {
        debug() << "dom[" << i << "]: ";
        for (auto d : dom[i]) {
            debug() << d << ", ";
        }
        debug() << '\n';
    }
    // -------- build dom tree --------
    debug() << "build dom tree...\n";
    auto topological_order_uniqueptr = std::make_unique<int[]>(basic_blocks.size());
    auto topological_order = topological_order_uniqueptr.get();
    {
        auto dom_by_uniqueptr = std::make_unique<std::unordered_set<int>[]>(basic_blocks.size());
        auto dom_by = dom_by_uniqueptr.get();
        for (int t = 0; t < basic_blocks.size(); t++) {
            for (auto f : dom[t]) {
                dom_by[f].insert(t);
            }
        }
        std::queue<int> backs;            // index of blocks who doms nothing
        std::unordered_set<int> unsolved; // index of blocks that never pushed into `backs`
        for (int i = 0; i < basic_blocks.size(); i++) {
            assert(dom_by[i].size() > 0);
            if (dom_by[i].size() == 1) {
                assert(*dom_by[i].begin() == i);
                backs.push(i);
            } else {
                unsolved.insert(i);
            }
        }
        int tporder = basic_blocks.size();
        while (!backs.empty()) {
            auto nodei = backs.front();
            backs.pop();
            topological_order[nodei] = --tporder;
            for (auto it = unsolved.begin(); it != unsolved.end();) {
                int i = *it;
                auto &db = dom_by[i];
                db.erase(nodei);
                if (db.size() == 1) {
                    assert(*dom_by[i].begin() == i);
                    backs.push(i);
                    auto it_ = it;
                    it++;
                    unsolved.erase(it_);
                } else {
                    it++;
                }
            }
        }
        assert(tporder == 0);
    }
    auto dom_tree_uniqueptr = std::make_unique<std::unordered_set<int>[]>(basic_blocks.size() + 1);
    auto dom_tree = dom_tree_uniqueptr.get() + 1;
    std::vector<int> idom(basic_blocks.size());
    for (int i = 0; i < basic_blocks.size(); i++) {
        int dom_n = -1; // if nothing doms it, let -1 doms it
        int tporder = -1;
        for (int j : dom[i]) {
            if (j == i)
                continue;
            assert(j >= 0 && j < basic_blocks.size());
            if (topological_order[j] > tporder) {
                tporder = topological_order[j];
                dom_n = j;
            }
        }
        dom_tree[dom_n].insert(i);
        idom[i] = dom_n;
        debug() << "idom[" << i << "] = " << dom_n << "\n";
    }

    // -------- build dominance frontier --------
    debug() << "compute dominance frontier...\n";
    std::vector<std::unordered_set<int>> df(basic_blocks.size());
    {
        // post order traverse dom_tree
        std::vector<int> reversed_traverse_order(basic_blocks.size());
        for (int i = 0; i < basic_blocks.size(); i++) {
            reversed_traverse_order[topological_order[i]] = i;
        }
        for (auto it = reversed_traverse_order.rbegin(); it != reversed_traverse_order.rend(); it++) {
            auto &n = basic_blocks[*it];
            // DFLocal
            for (auto y : n.succs) {
                if (idom[y] != *it) {
                    df[*it].insert(y);
                }
            }
            // DFUp
            for (auto z : dom_tree[*it]) {
                for (auto y : df[z]) {
                    if (idom[y] != *it) {
                        df[*it].insert(y);
                    }
                }
            }
        }
    }

    // -------- introduce phi --------
    debug() << "introduce phi...\n";
    for (auto x : address_taken_objs) {
        std::unordered_set<ICFGBB *> already_list;
        std::unordered_set<ICFGBB *> worklist;
        std::unordered_set<ICFGBB *> ever_on_worklist;
        for (auto &bb : basic_blocks) { // initialize worklist
            for (auto &inst : bb.inst_list) {
                if (inst->getType() == AbsInst::Store) {
                    auto store_inst = static_cast<StoreInst *>(inst.get());
                    for (auto &pt : store_inst->aux_pts) {
                        if (pt.first == x) {
                            goto BB_ASSIGNING_X;
                        }
                    }
                }
            }
            continue;
        BB_ASSIGNING_X:
            worklist.insert(&bb);
            ever_on_worklist.insert(&bb);
        }

        while (!worklist.empty()) {
            auto n = *worklist.begin();
            worklist.erase(worklist.begin());
            for (auto m_it : df[n - basic_blocks.data()]) {
                auto m = basic_blocks.data() + m_it;
                if (already_list.count(m) == 0) {
                    m->insertPhi(x);
                    already_list.insert(m);
                }
                if (ever_on_worklist.count(m) == 0) {
                    ever_on_worklist.insert(m);
                    worklist.insert(m);
                }
            }
        }
    }

    // -------- rename --------
    debug() << "rename...\n";
    std::unordered_map<ValOrObj, std::stack<int>, WeakHashOfValOrObj, WeakEqualOfValOrObj> rename_ids;
    for (auto obj : address_taken_objs) {
        rename_ids[obj].push(0);
    }
    int current_id = 0;
    for (int i = 0; i < basic_blocks.size(); i++) {
        if (idom[i] == -1) {
            _ssaRename(i, dom_tree, rename_ids, current_id);
        }
    }
}

void ICFG::_ssaRename(int bb_index, std::unordered_set<int> *dom_tree,
                      std::unordered_map<ValOrObj, std::stack<int>, WeakHashOfValOrObj, WeakEqualOfValOrObj> &rename_ids,
                      int &current_id) { // pre-order traverse
    auto gen_name = [&](ValOrObj &x) {
        auto id = ++current_id;
        rename_ids[x].push(id);
        x.ssa_rename_id = id;
    };
    assert(bb_index >= 0 && bb_index < basic_blocks.size());
    auto &bb = basic_blocks[bb_index];
    for (auto &inst_uptr : bb.inst_list) {
        switch (inst_uptr->getType()) {
        case AbsInst::Phi: {
            auto phi = static_cast<PhiInst *>(inst_uptr.get());
            gen_name(phi->val);
        } break;
        case AbsInst::Store: {
            auto store = static_cast<StoreInst *>(inst_uptr.get());
            for (auto &pair : store->aux_pts) {
                assert(address_taken_objs.count(pair.second) == 1);
                pair.second.ssa_rename_id = rename_ids.at(pair.second).top();
                gen_name(pair.first);
            }
        } break;
        case AbsInst::Load: {
            auto load = static_cast<LoadInst *>(inst_uptr.get());
            for (auto &obj : load->aux_pts) {
                obj.ssa_rename_id = rename_ids.at(obj).top();
            }
        }
        }
    }
    for (auto m : bb.succs) {
        int j;
        for (j = 0; j < basic_blocks[m].preds.size(); j++) {
            if (basic_blocks[m].preds[j] == bb_index) {
                break;
            }
        }
        assert(j < basic_blocks[m].preds.size());
        // bb is the jth pred
        for (auto &inst_uptr : basic_blocks[m].inst_list) {
            if (inst_uptr->getType() != AbsInst::Phi) {
                break;
            }
            auto phi = static_cast<PhiInst *>(inst_uptr.get());
            auto &xj = phi->src[j];
            xj.ssa_rename_id = rename_ids.at(xj).top();
        }
    }
    for (auto child : dom_tree[bb_index]) {
        _ssaRename(child, dom_tree, rename_ids, current_id);
    }
    for (auto &inst_uptr : bb.inst_list) {
        switch (inst_uptr->getType()) {
        case AbsInst::Phi: {
            auto phi = static_cast<PhiInst *>(inst_uptr.get());
            rename_ids.at(phi->val).pop();
        } break;
        case AbsInst::Store: {
            auto store = static_cast<StoreInst *>(inst_uptr.get());
            for (auto &pair : store->aux_pts) {
                rename_ids.at(pair.first).pop();
            }
        } break;
        }
    }
}

} // namespace icfg
